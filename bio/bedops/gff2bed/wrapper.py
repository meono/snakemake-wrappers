__author__ = "Emre Ozdemir"
__copyright__ = "Copyright 2019, Emre Ozdemir"
__email__ = ""
__license__ = "BSD"


from snakemake.shell import shell

log = snakemake.log_fmt_shell()

shell(
      "(gff2bed {snakemake.params}"
      " < {snakemake.input} > {snakemake.output})"
      " {log}"
     )
