__author__ = "Emre Ozdemir"
__copyright__ = "Copyright 2018, Emre Ozdemir"
__email__ = ""
__license__ = "BSD"


from snakemake.shell import shell

log = snakemake.log_fmt_shell()

shell(
      "readseq "
      "{snakemake.params} "
      "{snakemake.input} "
      "-o {snakemake.output} "
      "{log} "
     )
