__author__ = "Emre Ozdemir"
__copyright__ = "Copyright 2018, Emre Ozdemir"
__email__ = ""
__license__ = "BSD"


from snakemake.shell import shell

log = snakemake.log_fmt_shell()

shell(
      "bedtools getfasta {snakemake.params}"
      " -fi {snakemake.input.in1} -bed {snakemake.input.in2}"
      " -fo {snakemake.output}"
      " {log}"
     )
