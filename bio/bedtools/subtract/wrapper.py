__author__ = "Emre Ozdemir"
__copyright__ = "Copyright 2018, Emre Ozdemir"
__email__ = ""
__license__ = "BSD"


from snakemake.shell import shell

log = snakemake.log_fmt_shell(stdout=False)

shell(
      "bedtools subtract {snakemake.params}"
      " -a {snakemake.input.in1} -b {snakemake.input.in2}"
      " 1> {snakemake.output}"
      " {log}"
     )
