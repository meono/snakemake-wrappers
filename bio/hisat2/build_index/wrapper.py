__author__ = "Emre Ozdemir"
__copyright__ = "Copyright 2018, Emre Ozdemir"
__email__ = ""
__license__ = "BSD"

# based on Wibowo Arindrarto's version

from snakemake.shell import shell

# Placeholder for optional parameters
extra = snakemake.params.get("extra", "")
# Run log
log = snakemake.log_fmt_shell()


# Executed shell command
shell(
    "(hisat2-build {extra} "
    "-p {snakemake.threads} "
    "-f {snakemake.input} {snakemake.params.prefix}) "
    "{log} "
     )
