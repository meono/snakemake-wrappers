__author__ = "Emre Ozdemir"
__copyright__ = "Copyright 2018, Emre Ozdemir"
__email__ = ""
__license__ = "BSD"

# based on Wibowo Arindrarto"s version

from snakemake.shell import shell

# Placeholder for optional parameters
extra = snakemake.params.get("extra", "")
# Run log
log = snakemake.log_fmt_shell()

input_flags = []
r1 = snakemake.input.get("r1")
r2 = snakemake.input.get("r2")
rU = snakemake.input.get("rU")

error = False
if rU not in [None, []]: # rU is not listed in inputs > None.
                         # expand(...) returned an empty list > []
    input_flags.append("-U "+",".join(rU))

if (r1 not in [None, []]) and (r2 not in [None, []]):
    input_flags.append("-1 "+ (r1 if type(r1) == str else ",".join(r1)))
    input_flags.append("-2 "+ (r2 if type(r2) == str else ",".join(r2)))
elif (r1 not in [None, []]) or (r2 not in [None, []]):
    error = True

if (input_flags != []) and (not error):
    input_flags = " ".join(input_flags)
else:
    raise RuntimeError(
            """
            Please provide reads in one of the following forms:
            - rU = a single or a list of unpaired read file/s
            - r1 = a single or a list of R1 read file/s,
              r2 = a single or a list of R2 read file/s
            - r1 = a single or a list of R1 read file/s,
              r2 = a single or a list of R2 read file/s,
              rU = a single or a list of unpaired read/s file
             
            When using paired-end reads, number of R1 and R2 files must be  
            equal.
            """)

shell(
    "(hisat2 {extra} "
    "--threads {snakemake.threads} "
    "-x {snakemake.params.idx} "
    "{input_flags} "
    " --summary-file {snakemake.output[1]} "
    "1>{snakemake.output[0]}) "
    "{log} "
     )
